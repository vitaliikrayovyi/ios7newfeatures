//
//  Animationsios7Tests.m
//  Animationsios7Tests
//
//  Created by Vitalii Krayovyi on 8/15/13.
//  Copyright (c) 2013 Master of Code. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "EXModel.h"

@interface Animationsios7Tests : XCTestCase
{
    EXModel *model;
}

@end

@implementation Animationsios7Tests

- (void)setUp
{
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
    model = [[EXModel alloc] init];
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    model = nil;
    [super tearDown];
    
}

- (void)testExample
{
    XCTAssertNotNil(model, @"model not nil");
    XCTAssertNil([model getSomeCoolString:NO], @"Must pass");
    XCTAssertNotNil([model getSomeCoolString:YES], @"Must pass");
    
}

@end
