//
//  EXMoveRecognizer.h
//  Animationsios7
//
//  Created by Vitalii Krayovyi on 8/15/13.
//  Copyright (c) 2013 Master of Code. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EXMoveRecognizer : UIGestureRecognizer

@property (strong, nonatomic) UIDynamicAnimator *animator;

@end
