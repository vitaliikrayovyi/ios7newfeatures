//
//  EXModel.h
//  Animationsios7
//
//  Created by Vitalii Krayovyi on 8/16/13.
//  Copyright (c) 2013 Master of Code. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface EXModel : NSObject

-(NSString *)getSomeCoolString:(BOOL)toReturn;

@end
