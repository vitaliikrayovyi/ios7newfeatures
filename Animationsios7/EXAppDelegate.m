//
//  EXAppDelegate.m
//  Animationsios7
//
//  Created by Vitalii Krayovyi on 8/15/13.
//  Copyright (c) 2013 Master of Code. All rights reserved.
//

#import "EXAppDelegate.h"

typedef void(^bgFetchCompletion)(UIBackgroundFetchResult result);


@implementation EXAppDelegate
{
    bgFetchCompletion completionBlock;
    NSURLSession *downloadSession;
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Override point for customization after application launch.
    return YES;
}
							
- (void)applicationWillResignActive:(UIApplication *)application
{
    [[UIApplication sharedApplication] setMinimumBackgroundFetchInterval:15.];
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

#pragma mark - NSURLSSession download task delegate
-(void)URLSession:(NSURLSession *)session
     downloadTask:(NSURLSessionDownloadTask *)downloadTask
didFinishDownloadingToURL:(NSURL *)location
{
    NSString *data = [[NSString alloc] initWithContentsOfURL:location encoding:NSUTF8StringEncoding error:nil];
    NSLog(@"Data: %@", data);
    if (completionBlock)
    {
        completionBlock(UIBackgroundFetchResultNewData);
    }
}

-(void)URLSession:(NSURLSession *)session
     downloadTask:(NSURLSessionDownloadTask *)downloadTask
didResumeAtOffset:(int64_t)fileOffset
expectedTotalBytes:(int64_t)expectedTotalBytes
{
        NSLog(@"CALLED -didResumeAtOffset");
}

-(void)URLSession:(NSURLSession *)session
     downloadTask:(NSURLSessionDownloadTask *)downloadTask
     didWriteData:(int64_t)bytesWritten
totalBytesWritten:(int64_t)totalBytesWritten
totalBytesExpectedToWrite:(int64_t)totalBytesExpectedToWrite
{
    NSLog(@"Bytes written: %lli", totalBytesWritten);
}

-(void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task didCompleteWithError:(NSError *)error
{
    if (completionBlock)
    {
        completionBlock(UIBackgroundFetchResultFailed);
    }
}

#pragma mark - background fetching
- (void)application:(UIApplication *)application performFetchWithCompletionHandler:(void (^)(UIBackgroundFetchResult result))completionHandler
{
    if (!downloadSession)
    {
        NSURLSessionConfiguration *bgConf = [NSURLSessionConfiguration backgroundSessionConfiguration:@"_download_id"];
        downloadSession = [NSURLSession sessionWithConfiguration:bgConf delegate:self delegateQueue:nil];
    }

    
    NSURLSessionDownloadTask *dTask = [downloadSession downloadTaskWithURL:[NSURL URLWithString:@"http://last.fm"]];
    [dTask resume];
    completionBlock = completionHandler;
    
    
}

@end
