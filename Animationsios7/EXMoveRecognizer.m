//
//  EXMoveRecognizer.m
//  Animationsios7
//
//  Created by Vitalii Krayovyi on 8/15/13.
//  Copyright (c) 2013 Master of Code. All rights reserved.
//

#import "EXMoveRecognizer.h"

@interface EXMoveRecognizer()
{
}

@end

@implementation EXMoveRecognizer

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    NSLog(@"Began");
    
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [touches anyObject];
    CGPoint point = [touch locationInView:self.view.superview];
    CGPoint center = self.view.center;
    center.y = point.y;
    
    self.view.center = center;
    if (_animator)
    {
        [_animator updateItemUsingCurrentState:self.view];
    }

}

- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event
{

    
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    NSLog(@"Ended");

}

@end
