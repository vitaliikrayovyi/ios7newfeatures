//
//  EXModel.m
//  Animationsios7
//
//  Created by Vitalii Krayovyi on 8/16/13.
//  Copyright (c) 2013 Master of Code. All rights reserved.
//

#import "EXModel.h"

@implementation EXModel

-(NSString *)getSomeCoolString:(BOOL)toReturn
{
    return toReturn ? @"str" : nil;
}

@end
