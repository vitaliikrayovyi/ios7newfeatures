//
//  EXViewController.m
//  Animationsios7
//
//  Created by Vitalii Krayovyi on 8/15/13.
//  Copyright (c) 2013 Master of Code. All rights reserved.
//

#import "EXViewController.h"
#import "EXMoveRecognizer.h"

@interface EXViewController ()
{
    __weak IBOutlet UIView  *_biggerView;
    __weak IBOutlet UIView  *_smallerView;
    __weak IBOutlet UIImageView *_container;
    
    UIDynamicAnimator       *_animator;
}


- (IBAction)onTap:(UITapGestureRecognizer *)sender;

@end

@implementation EXViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidAppear:(BOOL)animated
{
    _animator = [[UIDynamicAnimator alloc] initWithReferenceView:self.view];

    UIAttachmentBehavior *att = [[UIAttachmentBehavior alloc] initWithItem:_smallerView attachedToItem:_biggerView];
    
    UICollisionBehavior *col = [[UICollisionBehavior alloc] initWithItems:@[_smallerView, _biggerView]];
    [col setCollisionMode:UICollisionBehaviorModeBoundaries];
    [col addBoundaryWithIdentifier:@"bounds" forPath:[UIBezierPath bezierPathWithRect:self.view.bounds]];
    
//    UISnapBehavior *snap = [[UISnapBehavior alloc] initWithItem:_smallerView snapToPoint:CGPointMake(140., 50.)];
//    UISnapBehavior *snapBig = [[UISnapBehavior alloc] initWithItem:_biggerView snapToPoint:CGPointMake(160., 22.)];
    UISnapBehavior *containerSnap = [[UISnapBehavior alloc] initWithItem:_container snapToPoint:CGPointMake(self.view.bounds.size.width / 2,
                                                                                                            self.view.bounds.size.height / 2 + (self.view.bounds.size.height - _container.bounds.size.height) / 2)];
    
    [_animator addBehavior:col];
    [_animator addBehavior:att];
    [_animator addBehavior:containerSnap];
//    [_animator addBehavior:snap];
//    [_animator addBehavior:snapBig];
    
    EXMoveRecognizer *move = [[EXMoveRecognizer alloc] init];
    move.animator = _animator;
    [_smallerView addGestureRecognizer:move];
    
    EXMoveRecognizer *moveBig = [[EXMoveRecognizer alloc] init];
    moveBig.animator = _animator;
    
    UIScreenEdgePanGestureRecognizer *sepGestureRecognizer = [[UIScreenEdgePanGestureRecognizer alloc] initWithTarget:self action:@selector(onEdgePan:)];
    [sepGestureRecognizer setEdges:UIRectEdgeRight];
    [self.view addGestureRecognizer:sepGestureRecognizer];

    [_biggerView addGestureRecognizer:moveBig];
    
    UIInterpolatingMotionEffect *motionEffect = [[UIInterpolatingMotionEffect alloc] initWithKeyPath:@"center.x" type:UIInterpolatingMotionEffectTypeTiltAlongHorizontalAxis];
    [_container addMotionEffect:motionEffect];
}

-(void)onEdgePan:(UIScreenEdgePanGestureRecognizer *)gestureRecognizer
{
    NSLog(@"panFromRightEdge");
    CGPoint newCenter = _container.center;
    newCenter.x -= 10;
    _container.center = newCenter;
    
    [_animator updateItemUsingCurrentState:_container];
}


- (IBAction)onTap:(UITapGestureRecognizer *)sender {
    [_animator updateItemUsingCurrentState:_smallerView];
}
@end
